# Workflow Convolution

[[_TOC_]]

## Description

Fundamentally, a little example to see how launching image processing functions in Dask cluster.
Concretely, LOFAR images are convolved with a Gaussian kernel. Two versions:

* **img_gauss_conv_multiprocessing.py** : using classical python module *Multiprocessing.py*

* **img_gauss_conv_dask.py**: using a *Dask cluster*

### <a name="parameters"></a> Input parameters

* **sigma**. Defining the Gaussian filter ((<span style="color:blue">optional</span>)). Default: (3,3)
* **dirIN**. Directory where the original images are stored ((<span style="color:blue">optional</span>)). Default: '.'
* **dirOUT**. Directory where the convolved images are saved ((<span style="color:blue">optional</span>)). Default: '.'
* **pattern**. pattern that fit the image name to convolve ((<span style="color:blue">optional</span>)). Default='*.fits' (all)
* **prefix**. prefix for the output convolved image name ((<span style="color:blue">optional</span>)). Default='conv_'

#### Specific to  workflow_convolution_multiprocessing:
* **ncpus**. Number of parallel processes used by Multiprocessing ((<span style="color:blue">optional</span>)). Default:2

#### Specific to workflow_convolution_dask:
* **ncores**. Number of cores used by Dask ((<span style="color:blue">optional</span>)). Default:2
* **ip_cluster**. Address of cluster with DASK ((<span style="color:blue">optional</span>)). Default: http://127.0.0.1:8888

### Ouput

* Set of convolved (fits) images saved in **dirOUT**


## How to run the task

The run.sh script will pull the latest container from the registry (no building is required), and execute the task.


## Directory structure

<pre>
├──jupyter
│       │
│       ├── img_gauss_conv_dask
│       │      │
│       │      ├── scripts
│       │      │        │
│       │      │        ├── img_gauss_conv_dask.ipynb
│       │      │        │
│       │      │        └── img.fits
│       │      │
│       │      └── Dockerfile
│       │
│       └── img_gauss_conv_multiprocessing
│             │
│             ├── scripts
│             │         │
│             │         ├── img_gauss_conv_multiprocessing.ipynb
│             │         │
│             │         └── img.fits
│             │
│             └── Dockerfile
│
├── scripts
│       │
│       ├── img_gauss_conv_dask.py
│       │
│       ├── img_gauss_conv_multiprocessing.py
│       │
│       ├── img.fits
│       │
│       └── pipeline.sh
│
├── Dockerfile
│
├── README.md
│
├── requeriments.txt
│
└── run.sh
</pre>


## Dependencies
* scikit-image
* dask==2022.12.1
* distributed==2022.12.1
* dask-gateway==2023.1.1
* astropy==5.2.2


##  Usage (see [parameters](#parameters))

### For img_gauss_conv_multiprocessing:
    ```
    $ ./img_gauss_conv_multiprocessing.py --help
    usage: img_gauss_conv_multiprocessing.py [-h] [--ncpus NCPUS] [--dirIN DIRIN] [--dirOUT DIROUT] [--pattern PATTERN] [--prefix PREFIX]

    image processing

    options:
    -h, --help         show this help message and exit
    --ncpus NCPUS      Number of cpus to use (default:1))
    --dirIN DIRIN      dir where the images lay
    --dirOUT DIROUT    dir where the convolved images are saved
    --pattern PATTERN  name pattern of the images to process
    --prefix PREFIX    name prefix of the convolved images
    ```
#### Examples

  * `./img_gauss_conv_multiprocessing.py`

  * `./img_gauss_conv_multiprocessing.py --ncpus 2 --dirIN . --dirOUT conv --pattern '*.fits' --prefix conv_`


### For img_gauss_conv_dask:
    ```
     $ ./img_gauss_conv_dask.py --help
    usage: img_gauss_conv_multiprocessing.py [-h] [--ncores NCORES] [--ip_cluster IP_CLUSTER][--dirIN DIRIN] [--dirOUT DIROUT] [--pattern PATTERN] [--prefix PREFIX]

    image processing

    options:
    -h, --help         show this help message and exit
    --ncpus NCORES     Number of cores to use (default:1))
    --ip_cluster       Cluster IP with DASK
    --dirIN DIRIN      dir where the images lay
    --dirOUT DIROUT    dir where the convolved images are saved
    --pattern PATTERN  name pattern of the images to process
    --prefix PREFIX    name prefix of the convolved images
    ```

    ```
#### Examples

  * `./img_gauss_conv_dask.py`

  * `./img_gauss_conv_dask.py --ip_cluster http://127.0.0.1:8888' --ncores 2 --dirIN . --dirOUT conv --pattern '*.fits' --prefix conv_`


## How to use a Jupyter notebook

To use a Jupyter notebook environment:

* Build the image and run
  * For *img_gauss_conv_multiprocessing*
    1. Change to the directory: `cd jupyter/img_gauss_conv_multiprocessing/`
    2. Build the image:  `docker build . -tag im_conv_multi`
    3. Run:
          ```
            sudo docker run -it --rm \
                        -p 8888:8888 \
                        -v "${PWD}":/home/jovyan/work \
                        im_conv_multi
           ```


  * For *workflow_convolution_multiprocessing*
    1. Change to the directory: `cd jupyter/img_gauss_conv_dask/`
    2. Build the image:  `docker build . -tag im_conv_dask`
    3. Run:
          ```
            sudo docker run -it --rm \
                        -p 8888:8888 \
                        -v "${PWD}":/home/jovyan/work \
                        -e IP_CLUSTER=ip_cluster  \
                        im_conv_dask
          ```

* Open in browse http://127.0.0.1:8888/lab?token=........

* Set the [*INPUT* parameters](#parameters)
    * sigma
    * dirIN
    * dirOUT
    * pattern
    * prefix
    * ncpus (workflow_convolution_multiprocessing)
      or
      ncores & ip_cluster (workflow_convolution_dask)
