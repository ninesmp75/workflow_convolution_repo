#!/bin/bash
# this script fixes permissions inside the docker container,
# downloads data, and runs a python fits2x3d script.

# fix permissions
chmod 755 * -R

# download data
#wget https://....../img.fits

# run the code
python3 img_gauss_conv_multiprocessing.py

#python3 img_gauss_conv_dask.py

