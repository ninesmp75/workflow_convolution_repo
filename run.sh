#!/bin/bash

# check to pull latest version, if needed
docker pull registry.gitlab.com/ska-telescope/src/src-workloads/img_gauss_conv:latest

# run container pipeline
docker run -it --rm --name img_gauss_conv -v "$(pwd)"/scripts:/scripts/ registry.gitlab.com/ska-telescope/src/src-workloads/img_gauss_conv:latest

