FROM ubuntu:22.04


ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /scripts/

COPY ./requirements.txt $WORKDIR

RUN apt-get update \
    && apt-get -y install --no-install-recommends  \
    python3.11 \
    python3-pip \
    python3.11-venv\
    wget

# Python3 requirements
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r /scripts/requirements.txt


ENTRYPOINT ["./pipeline.sh"]
